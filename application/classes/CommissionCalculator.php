<?php

class CommissionCalculator
{
    private $config;
    private $source = array();
    private $exchange_rates = [];

    public function __construct($config = null)
    {
        //set config before implement the class
        $this->config = is_null($config) ? new config\App() : new config\App($config);
    }

    public function setSourceFile($filename)
    {
        //load CSV from protected/file by filename
        $this->loadDataBySource($filename);
        return $this;
    }

    /**
     * check this source has available row and then run the calculator
     */
    public function calculateSource()
    {
        if (!is_null($this->source)) {
            foreach ($this->source as $source) {
                $this->calculate($source);
            }
        }
    }

    /**
     * @param $row
     * after get fee by rule and get exchange by rate in api calculate the fee from any exchange
     */
    protected function calculate($row)
    {
        $source_data = $this->getDataFromSourceRow($row);
        $row_fee = $this->getCommissionFee($source_data);
        $base_exchange = $this->config->getConfig('app.base_exchange_currency');
        $_to_base_rate = $this->getExchangeRateByApi($base_exchange, strtolower($source_data['currency']));
        $_row_amount = $source_data['amount'];
        if ($_to_base_rate !=false){
            $converted_currency_to_base = $_row_amount * $_to_base_rate;
            $row_fee = ($row_fee / 100) * $converted_currency_to_base;
            echo (number_format(round($row_fee, 2), 2)) . '<br>';
        }
    }

    /**
     * @param $base_currency
     * @param $currect_currency
     * @return false|mixed
     * this method check the api has available in class then return rate by base currency has loaded from config file
     * so if that currency has not available return false
     */
    protected function getExchangeRateByApi($base_currency, $currect_currency)
    {
        if (!isset($this->exchange_rates[$base_currency])) {
            $url = $this->config->getConfig('app.api_url');
            $currency_list = file_get_contents($url);
            $this->parseApiData($currency_list);
        }
        if (isset($this->exchange_rates[$base_currency][$currect_currency])) {
            return $this->exchange_rates[$base_currency][$currect_currency];
        } else {
            return false;
        }
    }

    /**
     * @param $api_data
     * get json from api result and then parse into exchange rate global variable to use on all website
     * we can change this code while use another api source
     */
    private function parseApiData($api_data)
    {
        $_api_result = [];
        $array_data = json_decode($api_data);
        $base = strtolower($array_data->base);
        foreach ($array_data->rates as $key => $item) {
            $_api_result[$base][strtolower($key)] = $item;
        }
        $this->exchange_rates = $_api_result;
    }

    /**
     * @param $source_data
     * @return array|false|int|mixed
     * calculate fee by condition has defined in task
     * use last week data and extra currency condition to calculation fee
     */
    protected function getCommissionFee($source_data)
    {
        $_final_fee = $this->config->getConfig($source_data['operation_type'] . '.' . $source_data['user_type'] . '.commission_present');

        $_currency = strtolower($source_data['currency']);
        $_extra_fee = $this->config->getConfig($source_data['operation_type'] . '.' . $source_data['user_type'] . '.extra_currency_fee');
        if (!is_null($_extra_fee) && isset($_extra_fee[$_currency])) {
            list($limit_count, $limit_cost) = $this->checkCommissionFeeLimit($source_data);
            if ($limit_count < $_extra_fee[$_currency]['free_fee_per_week'] && $limit_cost < $_extra_fee[$_currency]['free_fee_per_week_cost']) {
                return 0;
            } else {
                return $_extra_fee[$_currency]['commission_present'];
            }
        }

        $_transaction_free_fee = $this->config->getConfig($source_data['operation_type'] . '.' . $source_data['user_type'] . '.free_fee_per_week');
        $_transaction_free_fee_cost = $this->config->getConfig($source_data['operation_type'] . '.' . $source_data['user_type'] . '.free_fee_per_week_cost');
        //check has limit in week for transaction count and cost
        if ($_transaction_free_fee == null && $_transaction_free_fee_cost == null) {
            return $_final_fee;
        } elseif ($_transaction_free_fee == null && $_transaction_free_fee_cost != null) {
            $limited_per_week = $this->checkCommissionFeeLimit($source_data, 'cost');
            if ($limited_per_week > $_transaction_free_fee_cost) {
                return $_final_fee;
            }
        } elseif ($_transaction_free_fee != null && $_transaction_free_fee_cost == null) {
            $limited_per_week = $this->checkCommissionFeeLimit($source_data, 'count');
            if ($limited_per_week > $_transaction_free_fee) {
                return $_final_fee;
            }
        } elseif ($_transaction_free_fee != null && $_transaction_free_fee_cost != null) {
            list($limit_count, $limit_cost) = $this->checkCommissionFeeLimit($source_data);
            if ($limit_count > $_transaction_free_fee && $limit_cost > $_transaction_free_fee_cost) {
                return $_final_fee;
            }
        }
        return 0;
    }

    /**
     * @param $data
     * @param null $export_type
     * @param string $limit
     * @return array|int|mixed
     * @throws Exception
     * check commission limitation by week and etc.
     * check and return log of last data from user
     */
    protected function checkCommissionFeeLimit($data, $export_type = null, $limit = 'week')
    {
        $date = new DateTime($data['date']);
        $date_year = $date->format("Y");
        $date_weak = $date->format("W");
        if ($date->format('m') == 12 && $date->format('d') == 31) {
            $date_year++;
        }
        $start_end_week = $this->getStartAndEndDateInWeek($date_weak, $date_year);
        $record_count = 0;
        $record_cost = 0;
        foreach ($this->source as $row) {
            $row_data = $this->getDataFromSourceRow($row);
            if (($row_data['date'] >= $start_end_week['start'] && $row_data['date'] <= $start_end_week['end']) && $row_data['user_id'] = $data['user_id']) {
                $record_cost += $row_data['amount'];
                $record_count++;
            }
        }
        if ($export_type == 'count') {
            return $record_count;
        } elseif ($export_type == 'cost') {
            return $record_cost;
        }
        return [$record_count, $record_cost];
    }

    /**
     * @param $week
     * @param $year
     * @return array
     * this function return start and end date of number week
     */
    private function getStartAndEndDateInWeek($week, $year)
    {
        $currect_date = new DateTime();
        $currect_date->setISODate($year, $week);
        $_result['start'] = $currect_date->format($this->config->getConfig('app.date_format'));
        $currect_date->modify('+6 days');
        $_result['end'] = $currect_date->format($this->config->getConfig('app.date_format'));
        return $_result;
    }

    /**
     * @param $row
     * @return array
     * this function parse data has read from CSF file and make readable on code
     * we can arrange the column name in config file
     */
    protected function getDataFromSourceRow($row)
    {
        $field_order = explode(',', $this->config->getConfig('app.source_file_type_order'));
        $field_order = array_flip($field_order);
        $_result['date'] = $row[$field_order['date']];
        $_result['user_id'] = $row[$field_order['user_id']];
        $_result['user_type'] = $row[$field_order['user_type']];
        $_result['operation_type'] = $row[$field_order['operation_type']];
        $_result['currency'] = $row[$field_order['currency']];
        $_result['amount'] = $row[$field_order['amount']];
        return $_result;
    }

    /**
     * @param $source_file
     * @return $this|false|void
     * load data from CSV file and check the url and file type in config
     * and the return the array of data
     */
    public function loadDataBySource($source_file)
    {
        $csv_file = $this->config->getConfig('app.default_csv_path') . $source_file .
            $this->config->getConfig('app.source_file_type');
        if (file_exists($csv_file)) {
            $file_to_read = fopen($csv_file, 'r');
            if ($file_to_read !== false) {
                $csv_row = 0;
                $_csv_result = [];
                while (($data = fgetcsv($file_to_read, 10000, ',')) !== false) {
                    for ($csv_column = 0; $csv_column < count($data); $csv_column++) {
                        $_csv_result[$csv_row][$csv_column] = $data[$csv_column];
                    }
                    $csv_row++;
                }
                fclose($file_to_read);
                if (!empty($_csv_result)) {
                    $this->source = $_csv_result;
                } else {
                    $this->source = [];
                }
                return $this;
            }
            return false;
        }
    }

    /**
     * we can implement read data from Mysql and another dataSource to grow up the project
     */
    public function loadDataByMysql()
    {
        // @TODO We Can implement Fetch Data From Mysql Here
    }
}
