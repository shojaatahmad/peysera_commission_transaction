<?php

namespace config;

class App
{
    private $config;

    public function __construct($default_config = null)
    {
        if (is_null($default_config)) {
            $this->config = [
                'deposit' => [
                    'private' => [
                        'commission_present' => 0.03,
                        'free_fee_per_week' => null,
                        'free_fee_per_week_cost' => 1200,
                        'extra_currency_fee'=>null,
                    ],
                    'business' => [
                        'commission_present' => 0.03,
                        'free_fee_per_week' => null,
                        'free_fee_per_week_cost' => null,
                        'extra_currency_fee'=>null,
                    ]
                ],
                'withdraw' => [
                    'private' => [
                        'commission_present' => 0.3,
                        'free_fee_per_week' => null,
                        'free_fee_per_week_cost' => null,
                        'extra_currency_fee'=>[
                            'eur'=>[
                                'commission_present' => 0.3,
                                'free_fee_per_week' => 3,
                                'free_fee_per_week_cost' => 1000.00,
                                'use_all_amount'=>false,
                            ]
                        ],
                    ],
                    'business' => [
                        'commission_present' => 0.5,
                        'free_fee_per_week' => null,
                        'free_fee_per_week_cost' => null,
                        'extra_currency_fee'=>null,
                    ]
                ],
                'app' => [
                    'make_rounded' => true,
                    'base_exchange_currency'=>'eur',
                    'date_format'=>'Y-m-d',
                    'default_csv_path'=>dirname(__DIR__).'\protected\file\\',
                    'source_file_type'=>'.csv',
                    'source_file_type_order'=>'date,user_id,user_type,operation_type,amount,currency',
                    'api_url'=>'https://developers.paysera.com/tasks/api/currency-exchange-rates',
                    'default_source'=>'csv', // we can set mysql and csv file
                    'mysql_config'=>[
                        'mysql_address'=>'localhost',
                        'mysql_post'=>3306,
                        'mysql_username'=>'root',
                        'mysql_password'=>'',
                        'mysql_database'=>'',
                    ],
                ]
            ];
        } else {
            $this->config = $default_config;
        }

    }

    public function getConfig($config_name)
    {
        if (isset($config_name)) {
            $explode_config = explode('.', $config_name);
            $_config_result = $this->config;
            foreach ($explode_config as $config_level) {
                $_config_result = $_config_result[$config_level];
            }
            return $_config_result;

        } else {
            return false;
        }
    }

}